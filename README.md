###Easy Macros is an extremely basic cross-platform macro tool written in Java.###
It was created out of frustration with existing macro programs being powerful but difficult to learn quickly when you have a small task you'd like to automate.  Macros are stored in plain text files and can be quickly run from a menu in the system tray.  Macros can also be bound to keys and organized into "bind sets," allowing you to quickly enable or disable groups of binds.

#Installation#
Easy Macros is a single jar file.  You can place it anywhere you like and run it.  It is recommended to have it launch on startup.  On Windows you can do this by placing the jar in your startup directory (C:\Users\<user name>\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup).

#Usage#
The first time the program is run, it will create a .easymac directory in your home directory.  Here is where your macros and bind sets will be stored.  While running, Easy Macros will create a tape recorder icon in your system tray.  You can click it to open a menu which looks like this:**TODO: add image**  You can run any macro file you've written by selecting it from the "Play Macro" submenu.  You can enable or disable bind sets on or off by selecting them from the "Toggle Binds" submenu.  You can quickly open your .easymac directory by selecting "Open Macro Folder."

#Creating a macro#
To create a macro, create a new text file in your .easymac directory with the extension ".macro".  Edit this file with a text editor, placing one step on each line in the desired sequence.  Steps require a command and at least one argument separated by spaces.  Available commands are:

##keydown     <key>##
presses the specified key

##keyup       <key>##
releases the specified key

##keystroke   <key>##
shorthand that presses and immediately releases the specified key

##autotype    <text>##
types the specified text very quickly

##mousedown   <x position> <y position> <button>##
presses the specified button at the specified coordinates

##mouse up    <x position> <y position> <button>##
presreleases the specified button at the specified coordinates

##mouseclick  <x position> <y position> <button>##
shorthand that presses and immediately releases the specified button at the specified coordinates

##wait        <milliseconds>##
waits for the specified time (use this if your macro is running too fast for the program it's interacting with)

##run         <command>##
runs a command in the command prompt

Key arguments can be any alphanumeric character or one of the following words:


```
#!
control
shift
capslock
tab
escape
alt
command
space
f1
f2
f3
f4
f5
f6
f7
f8
f9
f10
f11
f12
backspace
enter
printscreen
scrolllock
pausebreak
insert
home
pageup
delete
end
pagedown
numlock
up
down
left
right
contextmenu
numpad0
numpad1
numpad2
numpad3
numpad4
numpad5
numpad6
numpad7
numpad8
numpad9
```

For mouse actions, the button argument must be one of the following words:
```
#!
left
right
middle
```

You can also use the "#" symbol at the start of a line to make a comment.

Here is an example macro which will open firefox and browse to some web pages:

```
#!
#launch firefox and wait for it to load
run firefox
wait 3000

#place caret in address bar
keydown control
keystroke l
keyup control

#firefox will get confused if we continue too quickly
#so add a wait if it can't keep up
wait 50

#navigate to reddit
autotype reddit.com
keystroke enter

wait 50

#open a new tab
keydown control
keystroke t
keyup control

wait 50

#navigate to youtube
autotype youtube.com
keystroke enter
```
If Easy Macros is running when you save a file with a syntax mistake, the tray icon will alert you.

#Creating a bindset#
Bindsets are written much the same way as macros.  Simply create a text file in your .easymac directory with the extension ".bindset" and write one bind on each line.  Binds consist of an alphanumeric character or word from the key list above, a space, and a macro to map it to.  If the above demo were saved as "foxy.macro", we can bind it to "0" key on the numberpad using the following bind:
```
#!
numpad0 foxy
```