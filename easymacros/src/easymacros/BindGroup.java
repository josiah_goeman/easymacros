package easymacros;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author josiah
 */
public class BindGroup
{

    private String fileName;
    //maps j native key codes (NOT java's VK codes) to the name of a macro
    private HashMap<Integer, String> bindMapping = new HashMap();
    private boolean enabled;

    public boolean loadFromFile(File input)
    {
        fileName = input.getName();

        Scanner scanner = null;
        try
        {
            scanner = new Scanner(input);
        } catch (FileNotFoundException e)
        {
            System.err.println(e);
            System.exit(99);
        }

        int lineNumber = 0;
        while (scanner.hasNextLine())
        {
            lineNumber++;
            String line = scanner.nextLine();

            //line is empty (no non-whitespace characters) or a comment
            if (line.trim().length() == 0 || line.startsWith("#"))
            {
                continue;
            }

            String keyString = line.substring(0, line.indexOf(" "));
            int keyCode = KeyboardHelper.vkToNativeKeyCode(KeyboardHelper.keyStringToKeyCode(keyString));

            String macroName = line.substring(line.indexOf(" ") + 1);

            if (keyCode != -1)
            {
                if (EasyMacros.macroExists(macroName))
                {
                    //System.out.println("Loading macro: " + macroName);
                    bindMapping.put(keyCode, line.substring(line.indexOf(" ") + 1));
                } else
                {
                    EasyMacros.displaySyntaxError(fileName, lineNumber, "\"" + macroName + "\" is not a valid macro file.");
                }
            } else
            {
                EasyMacros.displaySyntaxError(fileName, lineNumber, "\"" + keyString + "\" is not a valid key.");
            }
        }

        return true;
    }

    public String getFileName()
    {
        return fileName;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    public boolean isEnabled()
    {
        return enabled;
    }

    public String getMappedMacroName(int keyCode)
    {
        if (bindMapping.containsKey(keyCode))
        {
            return bindMapping.get(keyCode);
        } else
        {
            return null;
        }
    }
}
