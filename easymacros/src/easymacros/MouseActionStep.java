package easymacros;

import java.awt.Robot;
import java.io.IOException;

/**
 *
 * @author josiah
 */
public class MouseActionStep implements IMacroStep
{

    private int xPos, yPos, buttonCode;
    private boolean isPress;

    public MouseActionStep(boolean isPress)
    {
        this.isPress = isPress;
    }

    @Override
    public void execute(Robot rob)
    {
        rob.mouseMove(xPos, yPos);
        if (isPress)
        {
            rob.mousePress(buttonCode);
        } else
        {
            rob.mouseRelease(buttonCode);
        }
    }
    
    @Override
    public boolean fromFileLine(String arguments) {
        String[] argumentsArray = arguments.split(" ");
        try
        {
            xPos = Integer.parseInt(argumentsArray[0]);
            yPos = Integer.parseInt(argumentsArray[1]);
        } catch (NumberFormatException e)
        {
            return false;
        }
        
        buttonCode = KeyboardHelper.mouseStringToMouseCode(argumentsArray[2]);
        if(buttonCode == -1)
        {
            return false;
        }
        
        return true;
    }
    
    @Override
    public String toFileLine()
    {
        if(isPress)
        {
            return "mousedown " + xPos + " " + yPos + " " + buttonCode;
        }
        else
        {
            return "mouseup " + xPos + " " + yPos + " " + buttonCode;
        }
    }
}
