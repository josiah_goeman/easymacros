package easymacros;

import java.awt.Robot;

/**
 *
 * @author josiah
 */
public class KeyActionStep implements IMacroStep
{

    private int keyCode;
    private final boolean isPress;

    //parse constructor
    public KeyActionStep(boolean isPress)
    {
        this.isPress = isPress;
    }
    
    public KeyActionStep(int keyCode, boolean isPress)
    {
        this.keyCode = keyCode;
        this.isPress = isPress;
    }

    @Override
    public void execute(Robot rob)
    {
        if (isPress)
        {
            rob.keyPress(keyCode);
        } else
        {
            rob.keyRelease(keyCode);
        }
    }
    
    @Override
    public boolean fromFileLine(String arguments) {
        keyCode = KeyboardHelper.keyStringToKeyCode(arguments);
        return keyCode != -1;
    }

    @Override
    public String toFileLine()
    {
        if(isPress)
        {
            return EasyMacros.KEY_DOWN_COMMAND + "";
        }else
        {
            return EasyMacros.KEY_UP_COMMAND + "";
        }
    }
}
