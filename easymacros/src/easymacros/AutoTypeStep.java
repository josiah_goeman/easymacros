package easymacros;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Arrays;

/**
 *
 * @author josiah
 */
public class AutoTypeStep implements IMacroStep{
    char[] chars;
    int strokeDelay;

    @Override
    public void execute(Robot rob)
    {
        for(int i = 0; i < chars.length; i++)
        {
            int keyboardKeyCode = KeyboardHelper.keyStringToKeyCode(chars[i] + "");
            if(KeyboardHelper.charRequiresShift(chars[i]))
            {
                rob.keyPress(KeyEvent.VK_SHIFT);
                rob.keyPress(keyboardKeyCode);
                rob.keyRelease(keyboardKeyCode);
                rob.keyRelease(KeyEvent.VK_SHIFT);
            }
            else
            {
                rob.keyPress(keyboardKeyCode);
                rob.keyRelease(keyboardKeyCode);
            }
            
            rob.delay(strokeDelay);
        }
    }
    
    @Override
    public boolean fromFileLine(String arguments) {
        //potential trouble here; need to compensate for space?
        chars = arguments.toCharArray();
        return true;
    }

    @Override
    public String toFileLine()
    {
        return EasyMacros.AUTO_TYPE_COMMAND + " " + Arrays.toString(chars);
    }
}
