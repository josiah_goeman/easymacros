package easymacros;

import java.awt.Robot;

/**
 *
 * @author josiah
 */
public interface IMacroStep {
    public void execute(Robot rob);
    public boolean fromFileLine(String arguments);
    public String toFileLine();
}
