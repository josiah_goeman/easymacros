package easymacros;

import java.awt.Robot;
import java.io.IOException;

/**
 *
 * @author josiah
 */
public class RunCommandStep implements IMacroStep
{

    String command;

    public RunCommandStep(String command)
    {
        this.command = command;
    }

    @Override
    public void execute(Robot rob)
    {
        try
        {
            Runtime.getRuntime().exec(command);
        } catch (IOException e)
        {
            System.err.println("Unable to run command:");
            System.err.println(e);
        }
    }
    
    @Override
    public boolean fromFileLine(String line) {
        command = line;
        return true;
    }

    @Override
    public String toFileLine()
    {
        return EasyMacros.RUN_COMMAND + " " + command;
    }
}
