package easymacros;

import java.awt.AWTException;
import java.awt.Robot;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author josiah
 */
public class Macro
{
    //true when ANY macro is running
    private static boolean isMacroRunning;

    private String fileName;
    private ArrayList<IMacroStep> steps;

    public Macro()
    {
        steps = new ArrayList();
    }

    public boolean loadFromFile(File input)
    {
        fileName = input.getName();
        steps = new ArrayList();

        Scanner scanner = null;
        try
        {
            scanner = new Scanner(input);
        } catch (FileNotFoundException e)
        {
            System.exit(99);
        }

        int currentLineNumber = 0;
        while (scanner.hasNextLine())
        {
            String line = scanner.nextLine();
            currentLineNumber++;

            //line is empty (no non-whitespace characters) or a comment
            if (line.trim().length() == 0 || line.startsWith("#"))
            {
                continue;
            }

            //all commands require at least one argument
            if (!line.contains(" "))
            {
                EasyMacros.displaySyntaxError(fileName, currentLineNumber, "No arguments found");
                return false;
            }

            //space separates command from arguments
            String command = line.substring(0, line.indexOf(" "));
            String arguments = line.substring(line.indexOf(" ") + 1);

            switch (command)
            {
                case EasyMacros.KEY_DOWN_COMMAND:
                    KeyActionStep keyStep = new KeyActionStep(true);
                    if (keyStep.fromFileLine(arguments))
                    {
                        steps.add(keyStep);
                    } else
                    {
                        EasyMacros.displaySyntaxError(fileName, currentLineNumber, "usage: keydown <key>");
                        return false;
                    }
                    break;
                case EasyMacros.KEY_UP_COMMAND:
                    keyStep = new KeyActionStep(false);
                    if (keyStep.fromFileLine(arguments))
                    {
                        steps.add(keyStep);
                    } else
                    {
                        EasyMacros.displaySyntaxError(fileName, currentLineNumber, "usage: keyup <key>");
                        return false;
                    }
                    break;
                case EasyMacros.KEY_STROKE_COMMAND:
                    keyStep = new KeyActionStep(true);
                    if (keyStep.fromFileLine(arguments))
                    {
                        steps.add(keyStep);
                        //if we could parse it once we know we can do it again here
                        keyStep = new KeyActionStep(false);
                        keyStep.fromFileLine(arguments);
                        steps.add(keyStep);
                    } else
                    {
                        EasyMacros.displaySyntaxError(fileName, currentLineNumber, "usage: keystroke <key>");
                        return false;
                    }
                    break;
                case "autotype":
                    AutoTypeStep atStep = new AutoTypeStep();
                    atStep.fromFileLine(arguments);
                    //no need to check for syntax errors here since we can autotype any string
                    steps.add(atStep);
                    break;
                case EasyMacros.MOUSE_DOWN_COMMAND:
                    MouseActionStep mouseStep = new MouseActionStep(true);
                    if (mouseStep.fromFileLine(arguments))
                    {
                        steps.add(mouseStep);
                    } else
                    {
                        EasyMacros.displaySyntaxError(fileName, currentLineNumber, "usage: mousedown <x position> <y position> <button>");
                        return false;
                    }
                    break;
                case EasyMacros.MOUSE_UP_COMMAND:
                    mouseStep = new MouseActionStep(false);
                    if (mouseStep.fromFileLine(arguments))
                    {
                        steps.add(mouseStep);
                    } else
                    {
                        EasyMacros.displaySyntaxError(fileName, currentLineNumber, "usage: mouseup <x position> <y position> <button>");
                        return false;
                    }
                    break;
                case EasyMacros.MOUSE_CLICK_COMMAND:
                    mouseStep = new MouseActionStep(true);
                    if (mouseStep.fromFileLine(arguments))
                    {
                        steps.add(mouseStep);
                    } else
                    {
                        EasyMacros.displaySyntaxError(fileName, currentLineNumber, "usage: mouseclick <x position> <y position> <button>");
                        return false;
                    }
                    break;
                case EasyMacros.DELAY_COMMAND:
                    DelayStep delayStep = new DelayStep();
                    if (delayStep.fromFileLine(arguments))
                    {
                        steps.add(delayStep);
                    } else
                    {
                        EasyMacros.displaySyntaxError(fileName, currentLineNumber, "usage: wait <milliseconds>");
                        return false;
                    }
                    break;
                case EasyMacros.RUN_COMMAND:
                    RunCommandStep runCommandStep = new RunCommandStep(arguments);
                    steps.add(runCommandStep);
                    break;

                default:
                    EasyMacros.displaySyntaxError(fileName, currentLineNumber, "\"" + command + "\" is not a valid command.");
                    return false;
            }
        }

        return true;
    }

    public void addStep(IMacroStep step)
    {
        steps.add(step);
    }

    public void execute()
    {
        //there is no guarantee we will be able to use the robot from the calling thread
        //e.g. event dispatcher thread in actionlistener for menu item
        //so we make our own thread
        Thread macroThread = new Thread()
        {
            public void run()
            {
                isMacroRunning = true;
                try
                {
                    Robot rob = new Robot();
                    rob.setAutoWaitForIdle(true);
                    for (IMacroStep step : steps)
                    {
                        step.execute(rob);
                        rob.delay(10);//add a small delay to give programs time to react
                    }
                } catch (AWTException e)
                {
                    System.err.println("Could not create robot because: ");
                    System.err.println(e);
                    System.exit(99);
                }
                
                isMacroRunning = false;
            }
        };
        macroThread.start();
    }
    
    public static boolean getIsAnyRunning()
    {
        return isMacroRunning;
    }
}
