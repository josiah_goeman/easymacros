package easymacros;

import java.awt.Robot;

/**
 *
 * @author josiah
 */
public class DelayStep implements IMacroStep
{

    int time;

    @Override
    public void execute(Robot rob)
    {
        rob.delay(time);
    }
    
    @Override
    public boolean fromFileLine(String arguments) {
        try
        {
            time = Integer.parseInt(arguments);
        }
        catch(NumberFormatException e)
        {
            return false;
        }
        
        return true;
    }

    @Override
    public String toFileLine()
    {
        return EasyMacros.DELAY_COMMAND + " " + time;
    }
}
