package easymacros;

import java.awt.CheckboxMenuItem;
import java.awt.Desktop;
import java.awt.Frame;
import java.awt.Menu;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.Window.Type;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseListener;

/**
 *
 * @author josiah
 */
public class EasyMacros implements NativeKeyListener, NativeMouseListener
{

    static final String APPDATA_PATH = System.clearProperty("user.home") + "/.easymac";
    static final String MACRO_FILE_EXTENSION = ".macro";
    static final String BIND_GROUP_FILE_EXTENSION = ".bindgroup";
    static final String KEY_DOWN_COMMAND = "keydown";
    static final String KEY_UP_COMMAND = "keyup";
    static final String KEY_STROKE_COMMAND = "keystroke";
    static final String AUTO_TYPE_COMMAND = "autotype";
    static final String MOUSE_DOWN_COMMAND = "mousedown";
    static final String MOUSE_UP_COMMAND = "mouseup";
    static final String MOUSE_CLICK_COMMAND = "mouseclick";
    static final String DELAY_COMMAND = "wait";
    static final String RUN_COMMAND = "run";

    private static HashMap<String, Macro> macros = new HashMap();
    private static ArrayList<BindGroup> bindGroups = new ArrayList();

    public static void main(String[] args) throws InterruptedException
    {
        //ensure the macro directory exists
        File macroDirectory = new File(APPDATA_PATH);
        if (!macroDirectory.exists())
        {
            macroDirectory.mkdir();
        }

        //load all macros and bind groups in the macro directory and add them to the tray
        EasyMacros.loadFiles();
        populatePlayMenu();
        populateBindMenu();

        //there's a bug in jnativehook that causes issues on linux mint when adding a tray icon.
        //this bug right here: https://github.com/kwhat/jnativehook/issues/92
        //using another thread to make the tray seems to solve the issue
        Thread jnhIsSilly = new Thread()
        {
            @Override
            public void run()
            {
                makeTrayMenu();
            }
        };
        jnhIsSilly.start();

        //turn off console logging for jnativehook
        Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
        logger.setLevel(Level.OFF);
        logger.setUseParentHandlers(false);

        //register our hook
        try
        {
            GlobalScreen.registerNativeHook();
        } catch (NativeHookException ex)
        {
            System.err.println("There was a problem registering the native hook.");
            System.err.println(ex.getMessage());
            System.exit(1);
        }

        //add an instance of this class as the listener
        EasyMacros listener = new EasyMacros();
        GlobalScreen.addNativeKeyListener(listener);

        //clean up the hook when program closes
        Runtime.getRuntime().addShutdownHook(new Thread()
        {
            @Override
            public void run()
            {
                GlobalScreen.removeNativeKeyListener(listener);
                try
                {
                    System.out.println("Close request received.  Unregistering keyboard hook.");
                    GlobalScreen.unregisterNativeHook();
                    System.out.println("Successfully unregistered hook.  Bye!");
                } catch (NativeHookException e)
                {
                    System.err.println("Could not unregister hook; closing now anyway.  Sorry!");
                    System.err.println(e);
                }
            }
        });

        //wait for something interesting to happen
        monitorMacroFiles();
    }

    //watches the macro directory for changes and reparses all macros when they change
    //this should be the last thing the main thread calls since it will never return
    private static long lastModificationTime;

    private static void monitorMacroFiles()
    {
        WatchService watcher = null;
        try
        {
            watcher = FileSystems.getDefault().newWatchService();
            monitorFilesHelper(watcher, APPDATA_PATH);
            //Path path = Paths.get(APPDATA_PATH);
            //path.register(watcher, ENTRY_CREATE, ENTRY_MODIFY, ENTRY_DELETE);
        } catch (IOException e)
        {
            System.err.println("Could not register watch service; files will not auto-reload!");
            System.err.println(e);
            //System.exit(99);
        }

        while (true)
        {
            WatchKey key = null;
            try
            {
                //wait until an event occurs
                key = watcher.take();
            } catch (InterruptedException e)
            {
                System.err.println(e);
                System.exit(99);
            }

            //consume changes
            key.pollEvents();

            //only run this a max of ten times per second.
            //many text editors make several file operations when saving
            //and we only want to reload after it's done
            if (System.currentTimeMillis() - lastModificationTime > 100)
            {
                //wait for all operations to complete since this is the first
                //file operation detected
                try
                {
                    Thread.sleep(100);
                } catch (InterruptedException e)
                {
                }
                System.out.println("Detected file change...");

                //reparse all macro and bind files and add entries to menus
                loadFiles();
                populatePlayMenu();
                populateBindMenu();
            }

            //key must be reset after use to detect future events
            if (!key.reset())
            {
                System.err.println("Error: watch key could not be reset");
                System.exit(99);
            }

            lastModificationTime = System.currentTimeMillis();
        }
    }

    //recursively adds directories to the watch service
    private static void monitorFilesHelper(WatchService watcher, String stringPath) throws IOException
    {
        Paths.get(stringPath).register(watcher, ENTRY_CREATE, ENTRY_MODIFY, ENTRY_DELETE);
        
        for (File fileIterator : new File(stringPath).listFiles())
        {
            if (fileIterator.isDirectory())
            {
                monitorFilesHelper(watcher, fileIterator.getAbsolutePath());
            }
        }
    }

    public static void displaySyntaxError(String macroName, int lineNumber, String message)
    {
        //the first time this can get run the tray doesn't exist yet.
        //and we can't make the tray first because then we'd have to wait unneccessarily
        //for the tray creation thread to finish or risk jnh dying
        //i swear, the hoops i jump through...
        try
        {
            trayIcon.displayMessage("Syntax error in " + macroName, " On line " + lineNumber + ": " + message, TrayIcon.MessageType.INFO);
        } catch (NullPointerException e)
        {
        }
        System.err.println("Syntax error in " + macroName + " on line: " + lineNumber);
        System.err.println(message);
    }

    //reset internal data structures and load all macro and bind files
    private static void loadFiles()
    {
        System.out.println("(Re)loading files...");
        //macros need to be loaded first so bind group is able to check if macro names in file are valid
        macros = new HashMap();
        loadMacrosHelper(APPDATA_PATH);
        bindGroups = new ArrayList();
        loadBindGroupsHelper(APPDATA_PATH);
        System.out.println("Done loading.");
    }

    //recursively search for and load macro and bind files
    public static void loadMacrosHelper(String path)
    {
        //iterate over all files in macro directory
        File currentDirectory = new File(path);
        File[] filesInDirectory = currentDirectory.listFiles();
        for (File fileIterator : filesInDirectory)
        {
            //recurse through subdirectories
            if (fileIterator.isDirectory())
            {
                loadMacrosHelper(fileIterator.getAbsolutePath());
            }

            //macro files have the .macro extension
            if (fileIterator.getName().endsWith(MACRO_FILE_EXTENSION))
            {
                String fileName = fileIterator.getName();
                String macroName = fileName.substring(0, fileName.lastIndexOf("."));

                Macro fromFile = new Macro();
                boolean parseSuccess = fromFile.loadFromFile(fileIterator);
                //we can only run macros with correct syntax
                if (parseSuccess)
                {
                    //map macro's file name (sans extension) to our parsed macro
                    macros.put(macroName, fromFile);
                    System.out.println("Loaded macro: " + macroName);
                }
            }
        }
    }

    private static void loadBindGroupsHelper(String path)
    {
        //iterate over all files in macro directory
        File currentDirectory = new File(path);
        File[] filesInDirectory = currentDirectory.listFiles();
        for (File fileIterator : filesInDirectory)
        {
            //recurse through subdirectories
            if (fileIterator.isDirectory())
            {
                loadBindGroupsHelper(fileIterator.getAbsolutePath());
            }

            if (fileIterator.getName().endsWith(BIND_GROUP_FILE_EXTENSION))
            {
                String fileName = fileIterator.getName();
                String bindGroupName = fileName.substring(0, fileName.lastIndexOf("."));

                BindGroup fromFile = new BindGroup();
                boolean parseSuccess = fromFile.loadFromFile(fileIterator);
                if (parseSuccess)
                {
                    bindGroups.add(fromFile);
                    System.out.println("Loaded bind group: " + bindGroupName);
                }
            }
        }
    }

    public static boolean macroExists(String name)
    {
        return macros.containsKey(name);
    }

    private static Menu playMenu = new Menu("Play Macro");

    private static void populatePlayMenu()
    {
        playMenu.removeAll();
        for (String macroName : macros.keySet())
        {
            MenuItem macroItem = new MenuItem(macroName);
            macroItem.addActionListener((ActionEvent ae)
                    -> 
                    {
                        macros.get(macroName).execute();
            });
            playMenu.add(macroItem);
        }
    }

    private static Menu bindMenu = new Menu("Toggle Binds");

    private static void populateBindMenu()
    {
        bindMenu.removeAll();
        for (BindGroup bind : bindGroups)
        {
            String bindName = bind.getFileName();
            CheckboxMenuItem bindItem = new CheckboxMenuItem(bindName.substring(0, bindName.lastIndexOf(".")));
            bindItem.addItemListener((ItemEvent ie)
                    -> 
                    {
                        bind.setEnabled(bindItem.getState());
            });

            bindMenu.add(bindItem);
        }
    }

    private static TrayIcon trayIcon;

    public static void makeTrayMenu()
    {
        //make sure the system tray is supported
        if (!SystemTray.isSupported())
        {
            System.err.println("SystemTray is not supported");
            System.exit(99);
        }

        Frame menuFrame = new Frame();
        menuFrame.setUndecorated(true);
        menuFrame.setType(Type.POPUP);
        menuFrame.setVisible(true);
        menuFrame.setResizable(false);
        menuFrame.setLocation(-1, -1);  //keep the small dot off screen

        PopupMenu popup = new PopupMenu();
        trayIcon = null;
        try
        {
            trayIcon = new TrayIcon(ImageIO.read(new File("easymacros_tray_icon.png")));
        } catch (Exception e)
        {
            System.err.println("Could not load tray icon!  Quitting.");
            System.exit(99);
        }
        SystemTray tray = SystemTray.getSystemTray();

        MenuItem openDirectoryItem = new MenuItem("Open Macro Folder");
        openDirectoryItem.addActionListener((ActionEvent ae)
                -> 
                {
                    try
                    {
                        Desktop.getDesktop().open(new File(APPDATA_PATH));
                    } catch (IOException e)
                    {
                    }
        });
        if (!Desktop.isDesktopSupported())
        {
            openDirectoryItem.setEnabled(false);
        }

        MenuItem exitItem = new MenuItem("Exit");
        exitItem.addActionListener((ActionEvent ae)
                -> 
                {
                    System.exit(0);
        });

        popup.add(playMenu);
        popup.add(bindMenu);
        popup.add(openDirectoryItem);
        popup.addSeparator();
        popup.add(exitItem);

        trayIcon.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                menuFrame.add(popup);
                popup.show(menuFrame, e.getXOnScreen(), e.getYOnScreen());
            }
        });

        try
        {
            tray.add(trayIcon);
        } catch (Exception e)
        {
            System.err.println("TrayIcon could not be added.");
            System.exit(99);
        }
    }

    @Override
    public void nativeKeyPressed(NativeKeyEvent nke)
    {
        for (BindGroup bindGroup : bindGroups)
        {
            if (!bindGroup.isEnabled())
            {
                continue;
            }

            String macroName = bindGroup.getMappedMacroName(nke.getKeyCode());
            if (macroName != null && macros.containsKey(macroName))
            {
                //only run a macro if there are no others currently running
                //otherwise macros can trigger eachother and themselves and start a fork bomb
                if (!Macro.getIsAnyRunning())
                {
                    System.out.println("Running macro: " + macroName);
                    macros.get(macroName).execute();
                }
            }
        }
    }

    //stubs for the native listener interfaces
    @Override
    public void nativeKeyReleased(NativeKeyEvent nke)
    {
    }

    @Override
    public void nativeKeyTyped(NativeKeyEvent nke)
    {
    }

    @Override
    public void nativeMouseClicked(NativeMouseEvent nme)
    {
    }

    @Override
    public void nativeMousePressed(NativeMouseEvent nme)
    {
    }

    @Override
    public void nativeMouseReleased(NativeMouseEvent nme)
    {
    }
}
