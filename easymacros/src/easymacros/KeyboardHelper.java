package easymacros;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.HashMap;

/**
 *
 * @author josiah
 */
public class KeyboardHelper
{

    //maps special characters to the characters actually pressed to type them (holding shift)
    private static final HashMap<String, Integer> SPECIAL_TO_VK_CODE_MAP = new HashMap();

    static
    {
        SPECIAL_TO_VK_CODE_MAP.put("~", KeyEvent.VK_BACK_QUOTE);
        SPECIAL_TO_VK_CODE_MAP.put("!", KeyEvent.VK_1);
        SPECIAL_TO_VK_CODE_MAP.put("@", KeyEvent.VK_2);
        SPECIAL_TO_VK_CODE_MAP.put("#", KeyEvent.VK_3);
        SPECIAL_TO_VK_CODE_MAP.put("$", KeyEvent.VK_4);
        SPECIAL_TO_VK_CODE_MAP.put("%", KeyEvent.VK_5);
        SPECIAL_TO_VK_CODE_MAP.put("^", KeyEvent.VK_6);
        SPECIAL_TO_VK_CODE_MAP.put("&", KeyEvent.VK_7);
        SPECIAL_TO_VK_CODE_MAP.put("*", KeyEvent.VK_8);
        SPECIAL_TO_VK_CODE_MAP.put("(", KeyEvent.VK_9);
        SPECIAL_TO_VK_CODE_MAP.put(")", KeyEvent.VK_0);
        SPECIAL_TO_VK_CODE_MAP.put("_", KeyEvent.VK_MINUS);
        SPECIAL_TO_VK_CODE_MAP.put("+", KeyEvent.VK_EQUALS);
        SPECIAL_TO_VK_CODE_MAP.put("{", KeyEvent.VK_BRACELEFT);
        SPECIAL_TO_VK_CODE_MAP.put("}", KeyEvent.VK_BRACERIGHT);
        SPECIAL_TO_VK_CODE_MAP.put("|", KeyEvent.VK_BACK_SLASH);
        SPECIAL_TO_VK_CODE_MAP.put(":", KeyEvent.VK_SEMICOLON);
        SPECIAL_TO_VK_CODE_MAP.put("\"", KeyEvent.VK_QUOTE);
        SPECIAL_TO_VK_CODE_MAP.put("<", KeyEvent.VK_COMMA);
        SPECIAL_TO_VK_CODE_MAP.put(">", KeyEvent.VK_PERIOD);
        SPECIAL_TO_VK_CODE_MAP.put("?", KeyEvent.VK_SLASH);
    }

    private static final HashMap<String, Integer> KEYWORD_TO_VK_CODE_MAP = new HashMap();

    static
    {
        KEYWORD_TO_VK_CODE_MAP.put("control", KeyEvent.VK_CONTROL);
        KEYWORD_TO_VK_CODE_MAP.put("shift", KeyEvent.VK_SHIFT);
        KEYWORD_TO_VK_CODE_MAP.put("capslock", KeyEvent.VK_CAPS_LOCK);
        KEYWORD_TO_VK_CODE_MAP.put("tab", KeyEvent.VK_TAB);
        KEYWORD_TO_VK_CODE_MAP.put("escape", KeyEvent.VK_ESCAPE);
        KEYWORD_TO_VK_CODE_MAP.put("command", KeyEvent.VK_WINDOWS);
        KEYWORD_TO_VK_CODE_MAP.put("alt", KeyEvent.VK_ALT);
        KEYWORD_TO_VK_CODE_MAP.put("space", KeyEvent.VK_SPACE);
        KEYWORD_TO_VK_CODE_MAP.put("f1", KeyEvent.VK_F1);
        KEYWORD_TO_VK_CODE_MAP.put("f2", KeyEvent.VK_F2);
        KEYWORD_TO_VK_CODE_MAP.put("f3", KeyEvent.VK_F3);
        KEYWORD_TO_VK_CODE_MAP.put("f4", KeyEvent.VK_F4);
        KEYWORD_TO_VK_CODE_MAP.put("f5", KeyEvent.VK_F5);
        KEYWORD_TO_VK_CODE_MAP.put("f6", KeyEvent.VK_F6);
        KEYWORD_TO_VK_CODE_MAP.put("f7", KeyEvent.VK_F7);
        KEYWORD_TO_VK_CODE_MAP.put("f8", KeyEvent.VK_F8);
        KEYWORD_TO_VK_CODE_MAP.put("f9", KeyEvent.VK_F9);
        KEYWORD_TO_VK_CODE_MAP.put("f10", KeyEvent.VK_F10);
        KEYWORD_TO_VK_CODE_MAP.put("f11", KeyEvent.VK_F11);
        KEYWORD_TO_VK_CODE_MAP.put("f12", KeyEvent.VK_F12);
        KEYWORD_TO_VK_CODE_MAP.put("backspace", KeyEvent.VK_BACK_SPACE);
        KEYWORD_TO_VK_CODE_MAP.put("enter", KeyEvent.VK_ENTER);
        KEYWORD_TO_VK_CODE_MAP.put("contextmenu", KeyEvent.VK_CONTEXT_MENU);
        KEYWORD_TO_VK_CODE_MAP.put("printscreen", KeyEvent.VK_PRINTSCREEN);
        KEYWORD_TO_VK_CODE_MAP.put("scrolllock", KeyEvent.VK_SCROLL_LOCK);
        KEYWORD_TO_VK_CODE_MAP.put("pausebreak", KeyEvent.VK_PAUSE);
        KEYWORD_TO_VK_CODE_MAP.put("insert", KeyEvent.VK_INSERT);
        KEYWORD_TO_VK_CODE_MAP.put("home", KeyEvent.VK_HOME);
        KEYWORD_TO_VK_CODE_MAP.put("pageup", KeyEvent.VK_PAGE_UP);
        KEYWORD_TO_VK_CODE_MAP.put("delete", KeyEvent.VK_DELETE);
        KEYWORD_TO_VK_CODE_MAP.put("end", KeyEvent.VK_END);
        KEYWORD_TO_VK_CODE_MAP.put("pagedown", KeyEvent.VK_PAGE_DOWN);
        KEYWORD_TO_VK_CODE_MAP.put("up", KeyEvent.VK_UP);
        KEYWORD_TO_VK_CODE_MAP.put("down", KeyEvent.VK_DOWN);
        KEYWORD_TO_VK_CODE_MAP.put("left", KeyEvent.VK_LEFT);
        KEYWORD_TO_VK_CODE_MAP.put("right", KeyEvent.VK_RIGHT);
        KEYWORD_TO_VK_CODE_MAP.put("numpad0", KeyEvent.VK_NUMPAD0);
        KEYWORD_TO_VK_CODE_MAP.put("numpad1", KeyEvent.VK_NUMPAD1);
        KEYWORD_TO_VK_CODE_MAP.put("numpad2", KeyEvent.VK_NUMPAD2);
        KEYWORD_TO_VK_CODE_MAP.put("numpad3", KeyEvent.VK_NUMPAD3);
        KEYWORD_TO_VK_CODE_MAP.put("numpad4", KeyEvent.VK_NUMPAD4);
        KEYWORD_TO_VK_CODE_MAP.put("numpad5", KeyEvent.VK_NUMPAD5);
        KEYWORD_TO_VK_CODE_MAP.put("numpad6", KeyEvent.VK_NUMPAD6);
        KEYWORD_TO_VK_CODE_MAP.put("numpad7", KeyEvent.VK_NUMPAD7);
        KEYWORD_TO_VK_CODE_MAP.put("numpad8", KeyEvent.VK_NUMPAD8);
        KEYWORD_TO_VK_CODE_MAP.put("numpad9", KeyEvent.VK_NUMPAD9);
        KEYWORD_TO_VK_CODE_MAP.put("numlock", KeyEvent.VK_NUM_LOCK);
    }

    private static final HashMap<Integer, Integer> JAVA_VK_CODE_TO_J_NATIVE_CODE_MAP = new HashMap()
    {
        {
            put(0x07, 0x0000); // UNDEFINED
            put(0x08, 0x000E); // Backspace
            put(0x09, 0x000F); // Tab
            put(0x0C, 0xE04C); // Clear 
            put(0x0A, 0x001C); // Enter
            put(0x12, 0x0038); // Alt Key
            put(0x13, 0x0E45); // Pause
            put(0x14, 0x003A); // Caps Lock
            put(0x1B, 0x0001); // Escape
            put(0x20, 0x0039); // SpaceBar
            put(0x21, 0x0E49); // Page Up Key
            put(0x22, 0x0E51); // Page Down Key
            put(0x23, 0x0E4F); // End Key
            put(0x24, 0x0E47); // Home Key
            put(0x25, 0xE04B); // Left Arrow Key
            put(0x26, 0xE048); // Up Arrow Key
            put(0x27, 0xE04D); // Right Arrow Key
            put(0x28, 0xE050); // Down Arrow Key
            put(0x2C, 0x0E37); // Print Screen
            put(0x2D, 0x0E52); // Insert
            put(0x2E, 0x0E53); // Delete
            put(0x30, 0x000B); // 0
            put(0x31, 0x0002); // 1
            put(0x32, 0x0003); // 2
            put(0x33, 0x0004); // 3
            put(0x34, 0x0005); // 4
            put(0x35, 0x0006); // 5
            put(0x36, 0x0007); // 6
            put(0x37, 0x0008); // 7
            put(0x38, 0x0009); // 8
            put(0x39, 0x000A); // 9
            put(0x41, 0x001E); // A
            put(0x42, 0x0030); // B
            put(0x43, 0x002E); // C
            put(0x44, 0x0020); // D
            put(0x45, 0x0012); // E
            put(0x46, 0x0021); // F
            put(0x47, 0x0022); // G
            put(0x48, 0x0023); // H
            put(0x49, 0x0017); // I
            put(0x4A, 0x0024); // J
            put(0x4B, 0x0025); // K
            put(0x4C, 0x0026); // L
            put(0x4D, 0x0032); // M
            put(0x4E, 0x0031); // N
            put(0x4F, 0x0018); // O
            put(0x50, 0x0019); // P
            put(0x51, 0x0010); // Q
            put(0x52, 0x0013); // R
            put(0x53, 0x001F); // S
            put(0x54, 0x0014); // T
            put(0x55, 0x0016); // U
            put(0x56, 0x002F); // V
            put(0x57, 0x0011); // W
            put(0x58, 0x002D); // X
            put(0x59, 0x0015); // Y
            put(0x5A, 0x002C); // Z
            put(0x5B, 0x0E5B); // Windows
            put(0x5C, 0x0E5C); // Windows
            put(0x9D, 0x0E5B); // Command Key (Mac)
            put(0x5D, 0x0E5D); // Context Menu
            put(0x5F, 0xE05F); // Sleep
            put(0x60, 0x0052); // KP 0
            put(0x61, 0x004F); // KP 1
            put(0x62, 0x0050); // KP 2
            put(0x63, 0x0051); // KP 3
            put(0x64, 0x004B); // KP 4
            put(0x65, 0x004C); // KP 5
            put(0x66, 0x004D); // KP 6
            put(0x67, 0x0047); // KP 7
            put(0x68, 0x0048); // KP 8
            put(0x69, 0x0049); // KP 9
            put(0x6A, 0x0037); // KP Multiply
            put(0x6B, 0x004E); // KP Add
            put(0x6C, 0x0053); // KP Separator
            put(0x6D, 0x004A); // KP Subtract
            put(0x6F, 0x0E35); // KP Divide
            put(0x70, 0x003B); // F1
            put(0x71, 0x003C); // F2
            put(0x72, 0x003D); // F3
            put(0x73, 0x003E); // F4
            put(0x74, 0x003F); // F5
            put(0x75, 0x0040); // F6
            put(0x76, 0x0041); // F7
            put(0x77, 0x0042); // F8
            put(0x78, 0x0043); // F9
            put(0x79, 0x0044); // F10
            put(0x7A, 0x0057); // F11
            put(0x7B, 0x0058); // F12
            put(0x7C, 0x005B); // F13
            put(0x7D, 0x005C); // F14
            put(0x7E, 0x005D); // F15
            put(0x7F, 0x0063); // F16
            put(0x80, 0x0064); // F17
            put(0x81, 0x0065); // F18
            put(0x82, 0x0066); // F19
            put(0x83, 0x0067); // F20
            put(0x84, 0x0068); // F21
            put(0x85, 0x0069); // F22
            put(0x86, 0x006A); // F23
            put(0x87, 0x006B); // F24
            put(0x90, 0x0045); // NUM LOCK
            put(0x91, 0x0046); // SCROLL LOCK
            put(0xA0, 0x002A); // LSHIFT
            put(0xA1, 0x0036); // RSHIFT
            put(0xA2, 0x001D); // LCONTROL
            put(0xA3, 0x0E1D); // RCONTROL
            put(0xA6, 0xE06A); // Browser Back
            put(0xA7, 0xE069); // Browser Forward
            put(0xA8, 0xE067); // Browser Refresh
            put(0xA9, 0xE068); // Browser Stop
            put(0xAA, 0xE065); // Browser Search
            put(0xAB, 0xE066); // Browser Favorites
            put(0xAD, 0xE020); // Volume Mute
            put(0xAE, 0xE02E); // Volume Down
            put(0xAF, 0xE030); // Volume Up
            put(0xB0, 0xE019); // Media Next
            put(0xB1, 0xE010); // Previous Track
            put(0xB2, 0xE024); // Media Stop
            put(0xB3, 0xE022); // Media Play/Pause
            put(0xB4, 0xE06C); // Start Mail
            put(0xB5, 0xE06D); // Media Select
            put(0xBA, 0x0027); // ; key
            put(0xBB, 0x000D); // = key
            put(0xBC, 0x0033); // , key
            put(0xBD, 0x000C); // - key
            put(0xBE, 0x0034); // . key
            put(0xBF, 0x0035); // / key
            put(0xC0, 0x0029); // Back Quote
            put(0xDB, 0x001A); // [
            put(0xDD, 0x001B); // ]
            put(0xDC, 0x002B); // Backslash
            put(0xDE, 0x0028); // Quotes
        }
    };

    private static final HashMap<String, Integer> KEYWORD_TO_MOUSE_BUTTON_MASK = new HashMap();

    static
    {
        KEYWORD_TO_MOUSE_BUTTON_MASK.put("left", InputEvent.BUTTON1_MASK);
        KEYWORD_TO_MOUSE_BUTTON_MASK.put("right", InputEvent.BUTTON2_MASK);
        KEYWORD_TO_MOUSE_BUTTON_MASK.put("middle", InputEvent.BUTTON3_MASK);
    }

    public static int mouseStringToMouseCode(String mouseString)
    {
        if (KEYWORD_TO_MOUSE_BUTTON_MASK.containsKey(mouseString))
        {
            return KEYWORD_TO_MOUSE_BUTTON_MASK.get(mouseString);
        } else
        {
            return -1;
        }
    }

    //return whether the character requires holding shift to be typed
    public static boolean charRequiresShift(char character)
    {
        //upper case need shift
        if (Character.isUpperCase(character))
        {
            return true;
        }

        //special characters need shift
        return "~!@#$%^&*()_+{}|:\"<>?".contains(character + "");
    }

    //returns the keycode of the string
    public static int keyStringToKeyCode(String keyString)
    {
        if (keyString.length() > 1)
        {
            if (KEYWORD_TO_VK_CODE_MAP.containsKey(keyString))
            {
                return KEYWORD_TO_VK_CODE_MAP.get(keyString);
            }

            return -1;
        }

        if (SPECIAL_TO_VK_CODE_MAP.containsKey(keyString))
        {
            return SPECIAL_TO_VK_CODE_MAP.get(keyString);
        }

        return Character.toUpperCase(keyString.charAt(0));
    }

    public static int vkToNativeKeyCode(int vkCode)
    {
        if(JAVA_VK_CODE_TO_J_NATIVE_CODE_MAP.containsKey(vkCode))
        {
            return JAVA_VK_CODE_TO_J_NATIVE_CODE_MAP.get(vkCode);
        }
        else
        {
            return -1;
        }
    }
}
